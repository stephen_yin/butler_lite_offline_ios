	(function() {

		setupWebViewJavascriptBridge(function(bridge) {
			bridge.registerHandler('testJSFunction', function(data, responseCallback) {
				//					responseCallback('js执行过了');
			});
		});

	})();

	function setupWebViewJavascriptBridge(callback) {
		if(window.WebViewJavascriptBridge) {
			return callback(WebViewJavascriptBridge);
		}
		if(window.WVJBCallbacks) {
			return window.WVJBCallbacks.push(callback);
		}
		window.WVJBCallbacks = [callback];
		var WVJBIframe = document.createElement('iframe');
		WVJBIframe.style.display = 'none';
		WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__';
		document.documentElement.appendChild(WVJBIframe);
		setTimeout(function() {
			document.documentElement.removeChild(WVJBIframe)
		}, 0)
	}

	function setV(keyname, datas, callback) {
		callback = callback || function() {};
		var data = {};
		if(typeof datas == "object") {data[keyname] = JSON.stringify(datas)} else {data[keyname] = datas}
		console.log(window.WebViewJavascriptBridge);
		try {
				
			WebViewJavascriptBridge.callHandler('setLocalData', data, function(result) {
				callback(result);
			});
		} catch (error) {
			alert("存储失败");
		}
	}

	function getV(keyname, callback) {
		callback = callback || function() {};
		try {
			setTimeout(function() {				
				WebViewJavascriptBridge.callHandler('getLocalData', keyname, function(result) {
					console.log(typeof result);
					if(typeof result == "object"){ callback(result)}else{callback(JSON.parse(result))}
				});
			}, 100);
		} catch (error) {
			alert("读取失败");
		}
	}