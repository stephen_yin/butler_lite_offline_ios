    /**
     * 公共变量
     */    
    var APISERVER = "http://10.88.0.1:8008/";
    var PROJECT = "butler";
    var API_URL = APISERVER + PROJECT + "/?device_type=tablet&s=";
//var API_URL = "../?device_type=tablet&s=";
    var DEBUG = 1;

//	对接接口，获取系统配置
//var url0 = API_URL + "config.get_config&project="+PROJECT;
//	get_data(url0,"config").then(function(json){
//	});
//	获取UUID设备号
//window.webkit.messageHandlers.get_current_device_uuid_method_name.postMessage("get_current_device_uuid_method_name");
//function get_current_device_uuid(uuid_str) {
//	// uuid_str 为设备的 UUID  接下来进行数据请求
//	window.localStorage.setItem("UUID",uuid_str);
//}
//临时变量定义
// var config = JSON.parse(window.localStorage.getItem("config"));
var deviceid = window.localStorage.getItem("UUID");
//var deviceid = "E815A881-0509-4BC0-80C6-E13533F0631A";
var hotel_id  = '086test1’';
var restaurant_id = 'r003';
var room_id =  window.localStorage.getItem("room_id") ? window.localStorage.getItem("room_id") : '';
//console.log(room_id);
var device_id = deviceid;
var device_type = "tablet";
//var Date = new Date().getDate();
var device_name = "pad";
//处理每个命令的ajax请求以及回调函数  
function send_action(command, arg2) {  
    var dtd = $.Deferred();//定义deferred对象  
    $.post(  
        "url",  
        {  
            command: command,  
            arg2: arg2  
        }  
    ).done(function (json) {  
//      json = $.parseJSON(json);  
        //每次请求回调函数的处理逻辑  
        //  
        //  
        //  
        //逻辑结束  
        dtd.resolve();  
    }).fail(function (){  
        //ajax请求失败的逻辑     
        dtd.reject();  
    });  
    return dtd.promise();//返回Deferred对象的promise，防止在外部修改状态  
}  
	

/**
 * GET请求数据方法 封装的AJAX，适用于Butler,ISTV，门锁等APP.
 * @param {string} Url
 * @param {string} KeyName
 * @returns {unresolved}
 */
function get_data(Url, KeyName) {
    var defer = $.Deferred();
    $.ajax({
        type: "get",
        url: Url,
        dataType: "json",
        success: function (json) {

            if (json['ret'] === 200) {
                defer.resolve(json);
                
                if (KeyName)
                    localStorage.setItem(KeyName, JSON.stringify(json));
            } else {
                defer.reject(json);
                if(KeyName) 
                    localStorage.removeItem(KeyName);
            }
        }
    });
    return defer.promise();
}

/**
 * POST请求数据方法 封装的AJAX，适用于Butler,ISTV，门锁等APP.
 * @param {json} Datas
 * @param {string} Url
 * @param {string} KeyName
 * @returns {unresolved}
 */
function post_data(Datas, Url, KeyName) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: Url,
        data: Datas,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        success: function (json) {
            defer.resolve(json['data']);
          
            if (json['ret'] === 200) {
                defer.resolve(json);
                if (KeyName)
                    localStorage.setItem(KeyName, JSON.stringify(json));
            } else {
                defer.reject(json);
                if(KeyName) 
                    localStorage.removeItem(KeyName);
            }
        }
    });
    return defer.promise();
}
//正则传输页面后面的参数；
//用法：GetQueryString("参数名1")；
//用法：GetQueryString("参数名1")；
function GetQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');

    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}

//检测本地文件存在与否
	function fileExists(URL) {  
	    var http = new XMLHttpRequest();  
	    http.open('HEAD', URL, false);  
	    http.send();
	    return http.status;  
	} 
	
//价格处理函数	
function handlePrice(price){
	var newprice = price.substring(0,price.length-1);
	var priceone = newprice.substring(0,newprice.length-2);
	var pricetwo = newprice.substring(newprice.length-2,newprice.length);
	var priceall = priceone + "." + pricetwo;
	return priceall;
}


